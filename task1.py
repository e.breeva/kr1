﻿import sys
import math
# преобразуем строки в числа
def MyPrepareList(lines):
    numbers = []
    for x in lines:
        n = float(x.strip())
        numbers.append(n)
    return numbers
# суммируем список чисел
def MyCalcRow(numbers):
    return 42 + math.fsum([1 / n for n in numbers])
# открытие файла, чтение данных и запуск основной работы
def MyOpenFile():
    filename = sys.argv[len(sys.argv)-1]
    with open(filename) as f:
        lines=f.readlines()
    return(lines)
lines=MyOpenFile()
numbers = MyPrepareList(lines)
print("Сумма ряда: ", MyCalcRow(numbers))
input("Нажмите Enter для выхода...")
